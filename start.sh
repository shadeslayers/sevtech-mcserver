#!/bin/bash

set -e

#Get into our working directory.
cd /data

#Most of the files we need are packed into /tmp/ftb when the image is build, so we want to move them to our live directory, /data
cp -rf /tmp/ftb/* .

#Check to make sure they accepted EULA.
if [ "$EULA" = true ]; then
    echo "eula=true" > eula.txt
fi

#I don't know what this line was doing, so I commented it out.
#cp -rf /tmp/overrides/* .

#Checks for a server.properties file.
if [[ ! -e server.properties ]]; then
    cp /tmp/server.properties .
fi


#Below are environmental vars that are being placed into appropriate locations. Most of these are lines in server.propertie that alter a server setting.
#If the string is not empty, then we put whatever is in that string into a location.. basically.
#That -n basically checks to make sure the string is non-zero. After all, if you did not provide the env var, we don't want to override the default with a blank. 


if [[ -n "$MOTD" ]]; then
    sed -i "/motd\s*=/ c motd=$MOTD" /data/server.properties
fi
if [[ -n "$LEVEL" ]]; then
    sed -i "/level-name\s*=/ c level-name=$LEVEL" /data/server.properties
fi
if [[ -n "$FLIGHT" ]]; then
	sed -i "/allow-flight\s*=/ c allow-flight=$FLIGHT" /data/server.properties
fi
if [[ -n "$PORT" ]]; then
    sed -i "/server-port\s*=/ c server-port=$PORT" /data/server.properties
fi
if [[ -n "$DIFFICULTY" ]]; then
    sed -i "/difficulty\s*=/ c difficulty=$DIFFICULTY" /data/server.properties
fi
if [[ -n "$GAMEMODE" ]]; then
    sed -i "/gamemode\s*=/ c gamemode=$gamemode" /data/server.properties
fi
if [[ -n "$HARDCORE" ]]; then
    sed -i "/hardcore\s*=/ c hardcore=$HARDCORE" /data/server.properties
fi
if [[ -n "$SEED" ]]; then
    sed -i "/level-seed\s*=/ c level-seed=$SEED" /data/server.properties
fi
if [[ -n "$TYPE" ]]; then
    sed -i "/level-type\s*=/ c level-type=$TYPE" /data/server.properties
fi
if [[ -n "$HEIGHT" ]]; then
    sed -i "/max-build-height\s*=/ c max-build-height=$HEIGHT" /data/server.properties
fi
if [[ -n "$MAXPLAYERS" ]]; then
    sed -i "/max-players\s*=/ c max-players=$MAXPLAYERS" /data/server.properties
fi
if [[ -n "$ONLINE" ]]; then
    sed -i "/online-mode\s*=/ c online-mode=$ONLINE" /data/server.properties
fi
if [[ -n "$TIMEOUT" ]]; then
    sed -i "/player-idle-timeout\s*=/ c player-idle-timeout=$TIMEOUT" /data/server.properties
fi
if [[ -n "$PVP" ]]; then
    sed -i "/pvp\s*=/ c pvp=$PVP" /data/server.properties
fi
if [[ -n "$SPAWN" ]]; then
    sed -i "/spawn-protection\s*=/ c spawn-protection=$SPAWN" /data/server.properties
fi
if [[ -n "$NATIVETRANSPORT" ]]; then
    sed -i "/use-native-transport\s*=/ c user-native-transport=$NATIVETRANSPORT" /data/server.properties
fi
if [[ -n "$VIEWDISTANCE" ]]; then
    sed -i "/view-distance\s*=/ c view-distance=$VIEWDISTANCE" /data/server.properties
fi
if [[ -n "$WHITELIST" ]]; then
    sed -i "/white-list\s*=/ c white-list=$WHITELIST" /data/server.properties
fi

#Env vars for setting ops and whitelist below.
#If the list isn't empty, we want to basically replace the commas with new lines to make a proper list.

if [[ -n "$OPS" ]]; then
    echo $OPS | awk -v RS=, '{print}' >> ops.txt
fi
if [[ -n "$WL" ]]; then
    echo $WL | awk -v RS=, '{print}' >> whitelist.txt
fi



#wait 20 seconds, then start the server.
sleep 20
bash ./ServerStart.sh
