FROM java:8

RUN apt-get install -y wget unzip
RUN adduser --disabled-password --home=/data --uid 1234 --gecos "minecraft" minecraft

WORKDIR /tmp/ftb

RUN wget https://minecraft.curseforge.com/projects/sevtech-ages/files/2570735/download && unzip download && rm -f download
RUN chown -R minecraft /tmp/ftb && \
chmod +x Install.sh && \
./Install.sh && \
chmod +x ServerStart.sh && \
./ServerStart.sh && \
sed -i 's/false/TRUE/g' eula.txt

ADD start.sh /start 
RUN chmod +x /start

USER minecraft

VOLUME /data

ADD server.properties /tmp/server.properties
WORKDIR /data

CMD /start

ENV EULA="false"

ENV MOTD="This is the message of the day. Or it should be."
ENV LEVEL="world"
ENV FLIGHT="true"
ENV PORT=25565
ENV DIFFICULTY=1
ENV GAMEMODE=0
ENV HARDCORE="false"
ENV SEED=""
ENV TYPE="quark_realistic"
ENV HEIGHT=256
ENV MAXPLAYERS=20
ENV ONLINE="true"
ENV TIMEOUT=0
ENV PVP="true"
ENV SPAWN=16
ENV NATIVETRANSPORT="true"
ENV VIEWDISTANCE=10
ENV WHITELIST="false"

ENV OPS=""
ENV WL=""
