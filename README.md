# SevTech Minecraft Server

This is a dockerized version of a minecraft server that runs the SevTech modpack. I couldn't get the modpack to play nicely with the vanilla minecraft server docker image (too many mods?), so I decided to create my own. 

---
## How to use:
Basic docker command (simplest way to get it up and running):
`docker run --name SevtechContainerName -d -e EULA=true -p 25565:25565 shadeslayers211/sevtech-mcserver:1.0`  
By default we use port 25565, and we have to accept the EULA agreement. There is no 'latest' tag for this image, you must specify a version.  

### Example docker-compose:

[Example docker-compose.yml](https://bitbucket.org/shadeslayers/sevtech-mcserver/src/master/docker-compose.yml)

---
## Volumes
Everything is stored in /data, so if you created a persistent volume you can mount it with: 
`-v MinecraftData:/data`

---
## Environmental Variables

### Mandatory:
The EULA environmental var is REQUIRED.  
EULA - Boolean - Default is false. You MUST set this to true for the server to run. Setting this to true indicates you have read and agreed to the conditions set forth in the EULA, located at: https://account.mojang.com/documents/minecraft_eula   


### server.properties
**These alter settings in server.properties, which you can learn more about at: https://minecraft.gamepedia.com/Server.properties**

ENV|Type|Description
|--|--|--|
MOTD | String | Default is "A SevTech Minecraft server run on Docker". Sets the servers Message of the Day  
LEVEL | String | Default is "world". Sets the level-name. This is the name of the world, and subsequently the name of the folder.  
FLIGHT | Boolean | Default is true. Allows users to use flight while in Survival mode, if they have a mod that provides flight.  
PORT | Integer (1-65535) | Default is 25565. This is the port the server listens on. If you want people outside of LAN to connect, you will need to port forward.  
DIFFICULTY | Integer (1-4) | Default is 1. Changes difficulty. 0 = Peaceful. 1 = Easy. 2 = Normal. 3 = Hard.  
GAMEMODE | Integer (0-3) | Default is 0. Changes the gamemode. 0 = Survival. 1 = Creative. 2 = Adventure. 3 = Spectator.  
HARDCORE | Boolean | Default is false. If set to true, the server difficulty is ignored and set to Hard. If you die, you become a spectator.  
SEED | String | Default is blank. Add a seed to the world for world generation.  
TYPE | String | Default is quark_realistic. Determines the map type that is generated. You can view the vanilla types at the server.properties wiki page linked at the top of this section.  
HEIGHT | Integer | Default is 256. The maximum height in which building is allowed.  
MAXPLAYERS | Integer (0-2147483647) | Default is 20. Sets the maximum number of players allowed to play on the server at the same time.   
ONLINE | Boolean | Default is true. This determines whether or not the server will check connecting players against the Minecraft account database. Only set to false if you really know what you're doing, consider this a warning.  
TIMEOUT | Integer | Default is 0. Setting a value will kick any player that is idle for that many minutes.  
PVP | Boolean | Default is true. true = PvP. false = PvE.  
SPAWN | Integer | Default is 16. Determines radius of the spawn protection zone. 0 will make the zone a single block.   
NATIVETRANSPORT | Boolean | Default is true. This is for linux server performance improvements. Only set to false if you know what you're doing.   
VIEWDISTANCE | Integer (2-32) | Default is 10. Sets the amount of world data the server will send to the client. Measured in chunks in each direction of the player (radius, not diameter). 10 is recommended. There is a known mob spawn bug if you set it below 9. If you try a higher value and have lag issues, lower it back down.   
WHITELIST | Boolean | Default is false. When set to true, users not on the whitelist cannot login. If you set it to true, you will need to also use the WL environmental variable to add users to the whitelist.  




  
### OPS and Whitelisted users  

 ENV|Description|Example
|--|--|--|
OPS | This will define who on the server is given OP privileges. | OPS=User1,User2,User3 
WL | This will define who is allowed on the server, if WHITELIST is set to true. | WL=User1,User2,User3

---
## Image build version tags:

Rough roadmap of the versioning. This will be updated as progress is made and will provide general patch notes for completed versions.
Future versions will list planned features that are a WiP.


### Version <1.0

These can be considered alpha, as they are feature incomplete. The image itself works, however data persistence and environmental variables have not been setup.   

0.1: Initial push. This image won't do much for you.  
0.2: Image can now run a sevtech server. Not much more, no customization without opening a client into a container running the image.  
0.3: We can now set many server settings through the use of dockers environmental variables. Additionally, you now HAVE to set the EULA environmental to true in order for the server to run.  


### Version 1.0  

Feature complete.  
1. Image starts a minecraft server with the SevTech modpack.  
2. Image allows data persistence through use of the /data volume.  
3. Image allows manipulation of various server settings through the use of environmental variables.  


### Future Updates  
There are 3 major reasons that an update should be necessary:  
1. A bug is discovered.  
2. To fulfill a feature request.  
3. A new version of the sevtech server is released.  



---
